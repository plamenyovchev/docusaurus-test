/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React, { useEffect } from 'react';
import clsx from 'clsx';
import SkipToContent from '@theme/SkipToContent';
import AnnouncementBar from '@theme/AnnouncementBar';
import Navbar from '@theme/Navbar';
import Footer from '@theme/Footer';
import LayoutProviders from '@theme/LayoutProviders';
import LayoutHead from '@theme/LayoutHead';
import useKeyboardNavigation from '@theme/hooks/useKeyboardNavigation';
import './styles.css';
import window from 'global';

function Layout(props) {
  const {
    children,
    noFooter,
    wrapperclassName
  } = props;
  useKeyboardNavigation();

  useEffect(() => {
    function myScripts() {
      console.log('call', "myScripts()");
    };
    window.addEventListener('load', async () => {
      await import("cookieconsent");
      window.cookieconsent.initialise({
        revokeBtn: "<div class='cc-revoke'></div>",
        type: "opt-in",
        theme: "classic",
        // cookie: {
        //     domain: 'localhost'
        // },
        palette: {
          popup: {
            background: "#000",
            text: "#fff"
          },
          button: {
            background: "#fd0",
            text: "#000"
          }
        },
        content: {
          link: "Cookies",
          href: "https://2gdpr.com/cookies"
        },
        onInitialise: function (status) {
          if (status == cookieconsent.status.allow) myScripts();
        },
        onStatusChange: function (status) {
          if (this.hasConsented()) myScripts();
        }
      });
    });
  }, []);

  return (
    <LayoutProviders>
      <LayoutHead {...props} />
      <SkipToContent />
      <AnnouncementBar />
      <Navbar />
      <div className={clsx('main-wrapper', wrapperclassName)}>
        {children}
      </div>
      {!noFooter && <Footer />}
    </LayoutProviders>);
}

export default Layout;